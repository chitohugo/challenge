import requests

from api.models import Btc
from api import create_app

app = create_app()


def get_save_price_btc():
    """function you are looking for goes against an
    api and saves its response in the db.

    Args:

    Returns:
        Save or Exception.
    """
    with app.app_context():
        url = "https://cex.io/api/last_price/BTC/USD"
        try:
            response = requests.get(url)
            if response.status_code == 200:
                data = response.json()
                data["lprice"] = float(data["lprice"])
                Btc(**data).save()
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
