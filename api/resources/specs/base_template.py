# Create an APISpec
template = {
    "swagger": "2.0",
    "info": {
        "title": "Flask Restful Swagger Challenge",
        "description": "A Demof for the Challenge",
        "version": "0.1.1",
    },
    "securityDefinitions": {
        "Bearer": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header",
            "description": "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\""
        }
    },
    "security": [
        {
            "Bearer": []
        }
    ]

}
