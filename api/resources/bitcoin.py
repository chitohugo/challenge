from flasgger import swag_from
from flask import current_app
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse, Api

from api.commons.error_handling import ObjectNotFound
from api.models import Btc


class Bitcoin(Resource):
    @staticmethod
    @jwt_required()
    @swag_from('specs/search_price.yml')
    def get():
        """Method to find a resource in the database.

        Args:
          timestamp: Timestamp to search the db

        Returns:
          a Pagination object with the data using the filter or all records.
        """
        api = Api(current_app)
        parser = reqparse.RequestParser()
        parser.add_argument('page', 1, type=int, location=['args', 'json'])
        parser.add_argument('per_page', 25, type=int, location=['args', 'json'])
        parser.add_argument('timestamp',
                            type=str,
                            location=['args', 'json']
                            )
        kwargs = parser.parse_args()
        response = Btc.find_by_date(**kwargs)
        if not response.items:
            raise ObjectNotFound("Date not found")

        next_url = api.url_for(Bitcoin, page=response.next_num)
        prev_url = api.url_for(Bitcoin, page=response.prev_num)
        data_res = []
        out = []

        for item in response.items:
            data = {
                'price': str(item.lprice),
                'date': str(item.created_at)
            }
            data_res.append(data)
        out.append({
            'data': data_res,
            'next': next_url,
            'prev': prev_url,
            'total': response.total,
            'limit': 10,
            'pages': response.pages,
        })
        return out, 200


class BitcoinDetail(Resource):
    @staticmethod
    @jwt_required()
    @swag_from('specs/average_price.yml')
    def get():
        """Method to search for a range of dates the average
        and percentage difference.

        Args:
          start_date: Timestamp to search the db
          end_date: Timestamp to search the db

        Returns:
          a Pagination object with the data using the filter of dates.
        """
        api = Api(current_app)
        parser = reqparse.RequestParser()
        parser.add_argument('page', 1, type=int, location=['args', 'json'])
        parser.add_argument('per_page', 25, type=int, location=['args', 'json'])
        parser.add_argument('start_date',
                            type=str,
                            location=['args', 'json']
                            )
        parser.add_argument('end_date',
                            type=str,
                            location=['args', 'json']
                            )
        kwargs = parser.parse_args()
        response = Btc.find_by_dates(**kwargs)
        if not response.items:
            raise ObjectNotFound("Dates not found")

        next_url = api.url_for(BitcoinDetail, page=response.next_num)
        prev_url = api.url_for(BitcoinDetail, page=response.prev_num)
        out = []

        _avg = response.items[0].average
        _max = response.items[0].maximum
        percentage = 100 * abs(_avg - _max) / abs(_max)
        data_res = {
            'average': f"{_avg:.1f}",
            'maximum': f"{_max:.1f}",
            'percentage': f"{percentage:.2f}%"
        }
        out.append({
            'data': data_res,
            'next': next_url,
            'prev': prev_url,
            'total': response.total,
            'limit': 10,
            'pages': response.pages,
        })
        return out, 200
