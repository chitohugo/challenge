from flasgger import swag_from
from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token
from api.models import User


class Login(Resource):
    @staticmethod
    @swag_from('specs/login.yml')
    def post():
        """given the arguments: username and password return a access_token"""

        parser = reqparse.RequestParser()
        parser.add_argument('username',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        parser.add_argument('password',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        args = parser.parse_args()
        user = User.find_by_username(args['username'])

        if user and user.verify_password(args['password']):
            access_token = create_access_token(identity=user.id, fresh=True)
            return {'access_token': access_token}, 200

        return {'message': 'Invalid Credentials'}, 401


class Register(Resource):
    """given the arguments: username, password and email return
    a access_token and user created"""

    @staticmethod
    @swag_from('specs/register.yml')
    def post():
        parser = reqparse.RequestParser()
        parser.add_argument('username',
                            type=str,
                            required=True,
                            help='This field cannot be blank.'
                            )
        parser.add_argument('password',
                            type=str,
                            required=True,
                            help='This field cannot be blank.'
                            )
        parser.add_argument('email',
                            type=str,
                            required=True,
                            help='This field cannot be blank.')
        args = parser.parse_args()
        if User.find_by_email(args['email']) or User.find_by_username(args['username']):
            return {'message': 'There is a user with this email or username'}, 400

        user = User(**args)
        user.save()
        access_token = create_access_token(identity=user.id, fresh=True)
        return {'access_token': access_token}, 201
