import hashlib
import uuid
from datetime import datetime

from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

db = SQLAlchemy()
jwt = JWTManager()


class BaseModel:
    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class User(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=True)
    pwd_salt = db.Column(db.String(32), nullable=True)

    @property
    def password(self):
        """
        Prevents password from being accessed
        """
        raise AttributeError("Password is not a readable attribute.")

    @password.setter
    def password(self, password):
        """
        Sets a password to a hashed password
        """
        salt = uuid.uuid4().hex
        self.pwd_salt = salt
        self.password_hash = hashlib.sha512(
            password.encode('utf-8') + salt.encode('utf-8')).hexdigest()

    def verify_password(self, password):
        """
        Checks if hashed password matches actual password
        """
        if self.password_hash == hashlib.sha512(
                password.encode('utf-8') + self.pwd_salt.encode(
                    'utf-8')).hexdigest():
            return True

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @jwt.user_lookup_loader
    def user_loader_callback(self, jwt_data):
        user = User.query.get(jwt_data["sub"])
        if not user:
            return None
        return user

    def __repr__(self):
        return f'{self.email}'


def remove_millisecond():
    return datetime.utcnow().isoformat(timespec='seconds')


class Btc(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    lprice = db.Column(db.Float, nullable=False)
    curr1 = db.Column(db.String(80), nullable=False)
    curr2 = db.Column(db.String(80), nullable=False)
    created_at = db.Column(db.DateTime(),
                           default=remove_millisecond)

    @classmethod
    def find_by_date(cls, **kwargs):
        if not kwargs['timestamp']:
            query = cls.query
        else:
            date_time = datetime.strptime(kwargs['timestamp'],
                                          '%Y-%m-%d %H:%M:%S')
            query = cls.query.filter(cls.created_at == date_time)

        return query.paginate(per_page=kwargs['per_page'],
                              page=kwargs['page'],
                              error_out=True)

    @classmethod
    def find_by_dates(cls, **kwargs):
        start_date = datetime.strptime(kwargs['start_date'],
                                       '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime(kwargs['end_date'],
                                     '%Y-%m-%d %H:%M:%S')

        data = cls.query.with_entities(func.avg(cls.lprice).label("average"),
                                       func.max(cls.lprice).label("maximum")). \
            filter(cls.created_at.between(start_date, end_date))

        return data.paginate(per_page=kwargs['per_page'],
                             page=kwargs['page'],
                             error_out=True)
