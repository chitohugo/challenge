from celery.utils.log import get_task_logger
from flasgger import Swagger
from flask import Flask
from flask_migrate import Migrate

from api.endpoints import routes
from .celery import make_celery
from .commons.error_handling import register_error_handlers
from .models import db, jwt
from .resources.specs.base_template import template

migrate = Migrate()

logger = get_task_logger(__name__)


class Config:
    """App configuration."""

    SCHEDULER_API_ENABLED = True
    SQLALCHEMY_DATABASE_URI = \
        'postgresql://postgres:2021wenance@postgres/challenge'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    JWT_SECRET_KEY = 'WENANCE.2@2@2!'
    JWT_ACCESS_TOKEN_EXPIRES = False


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())
    app.config.update(
        result_backend='redis://redis:6379',
        broker_url='redis://redis:6379',
        beat_schedule={
            # Go to https://cex.io/api/last_price/BTC/USD every 10
            'recurrent_task-very_10s': {
                'task': 'recurrent_task',
                'schedule': 10.0
            }
        }
    )
    app.config['SWAGGER'] = {
        'title': 'Challenge Wenance',
        'uiversion': 3
    }

    db.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)
    routes(app)
    register_error_handlers(app)
    Swagger(app, template=template)
    return app


app = create_app()

celery = make_celery(app)


@celery.task(name="recurrent_task")
def recurrent_task():
    logger.info("Calling the service get_save_price_btc")
    from .services.request_btc import get_save_price_btc
    get_save_price_btc()
