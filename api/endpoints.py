from flask_restful import Api

from api.resources.user import Register, Login
from api.resources.bitcoin import Bitcoin, BitcoinDetail


def routes(app):
    api = Api(app)
    api.add_resource(Login, '/login')
    api.add_resource(Register, '/register')
    api.add_resource(Bitcoin, '/search_price')
    api.add_resource(BitcoinDetail, '/average_price')
