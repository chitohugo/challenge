# API for Challenge
## Running with docker

### Pre-requisites:
- docker
- docker-compose

### Steps   
1. Build with `docker-compose build`
2. Run with `docker-compose up`
3. Run Migrations:
   1. `docker exec -it challenge bash`
   2. `flask db migrate`
   3. `flask db upgrade`
4. Go to `http://localhost:5000/apidocs/` or use rest client.    


### Endpoints available
1. Register to create a user. You must supply username, password and email   
   `/register`
2. Log in to obtain a token and be able to use the endpoints   
   `/login`
3. Search bitcoin price by date and time   
   `/search-price`
4. Get the average and the percentage difference of bitcoin for certain dates  
   `/average-price`   